-- MySQL Script generated by MySQL Workbench
-- Thu Dec 20 12:42:39 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema web-semestralka
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `web-semestralka` ;

-- -----------------------------------------------------
-- Schema web-semestralka
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `web-semestralka` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci ;
USE `web-semestralka` ;

-- -----------------------------------------------------
-- Table `web-semestralka`.`authorization`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web-semestralka`.`authorization` ;

CREATE TABLE IF NOT EXISTS `web-semestralka`.`authorization` (
  `idAuthorization` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(16) NOT NULL,
  `value` INT NOT NULL,
  PRIMARY KEY (`idAuthorization`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web-semestralka`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web-semestralka`.`user` ;

CREATE TABLE IF NOT EXISTS `web-semestralka`.`user` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `ban` TINYINT NOT NULL,
  `idAuthorization` INT NOT NULL,
  PRIMARY KEY (`idUser`),
  CONSTRAINT `fk_Users_Authorization`
    FOREIGN KEY (`idAuthorization`)
    REFERENCES `web-semestralka`.`authorization` (`idAuthorization`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web-semestralka`.`contribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web-semestralka`.`contribution` ;

CREATE TABLE IF NOT EXISTS `web-semestralka`.`contribution` (
  `idContribution` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NOT NULL,
  `movieTitle` VARCHAR(45) NOT NULL,
  `content` TEXT NOT NULL,
  `public` TINYINT NOT NULL,
  `assigned` TINYINT NOT NULL,
  `declined` TINYINT NOT NULL,
  `fileName` VARCHAR(64) NOT NULL,
  `addDate` DATETIME NOT NULL,
  `ratingsAmount` INT NULL,
  `overallRating` FLOAT NULL,
  PRIMARY KEY (`idContribution`),
  CONSTRAINT `fk_Contributions_Users1`
    FOREIGN KEY (`idUser`)
    REFERENCES `web-semestralka`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web-semestralka`.`rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `web-semestralka`.`rating` ;

CREATE TABLE IF NOT EXISTS `web-semestralka`.`rating` (
  `idRating` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NOT NULL,
  `idContribution` INT NOT NULL,
  `theme` INT NULL,
  `originality` INT NULL,
  `language` INT NULL,
  `overall` FLOAT NULL,
  PRIMARY KEY (`idRating`),
  CONSTRAINT `fk_Ratings_Users1`
    FOREIGN KEY (`idUser`)
    REFERENCES `web-semestralka`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ratings_contributions1`
    FOREIGN KEY (`idContribution`)
    REFERENCES `web-semestralka`.`contribution` (`idContribution`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `web-semestralka`.`authorization`
-- -----------------------------------------------------
START TRANSACTION;
USE `web-semestralka`;
INSERT INTO `web-semestralka`.`authorization` (`idAuthorization`, `name`, `value`) VALUES (1, 'Administrator', 10);
INSERT INTO `web-semestralka`.`authorization` (`idAuthorization`, `name`, `value`) VALUES (2, 'Reviewer', 5);
INSERT INTO `web-semestralka`.`authorization` (`idAuthorization`, `name`, `value`) VALUES (3, 'Author', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `web-semestralka`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `web-semestralka`;
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Jakub Kodera', 'admin', '$2y$10$I.8XO9uHLyzr/K0cwvKYI.J96jCV6b3nszGokmzF/4A3CxOOthrhe', 'kuba.kodera@seznam.cz', 0, 1);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Test User', 'test', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'test@test.com', 0, 2);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Novy Uzivatel', 'novy', '$2y$10$xtUi0I5zp.dNF2rgeHfJLORa.CcagyJM9TO22Dn.AbTsLG629v/yu', 'novy@novy.com', 0, 3);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Recenzent Pepa', 'pepa', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'pepa@gmail.com', 0, 2);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Recenzent Franta', 'franta', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'franta@gmail.com', 0, 2);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Recenzent Jiri', 'jiri', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'jiri@gmail.com', 0, 2);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Recenzent Ondra', 'ondra', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'ondra@gmail.com', 0, 2);
INSERT INTO `web-semestralka`.`user` (`idUser`, `name`, `login`, `password`, `email`, `ban`, `idAuthorization`) VALUES (null, 'Recenzent Filip', 'filip', '$2y$10$eeups6bF0W76e08IFzoVxOJrf.EJ0AnEUsBK4m0mQI.0DENy47ADq', 'filip@gmail.com', 0, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `web-semestralka`.`contribution`
-- -----------------------------------------------------
START TRANSACTION;
USE `web-semestralka`;
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (2, 3, 'Testovní příspěvek', 'Příspěvek ve kterém nic není.', 1, 1, 0, 'oop-du-01.pdf', '2018-12-20 11:04:35', 3, 3.88889);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (3, 3, 'Další příspěvek', 'Opět příspěvek ve kterém nic není.', 0, 0, 1, 'oop-du-02.pdf', '2018-12-20 11:05:12', 0, NULL);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (4, 3, 'Deadpool', 'Krátký popisek o filmu který jsem nedávno viděl.', 0, 0, 0, 'oop-du-03.pdf', '2018-12-20 11:05:49', 0, NULL);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (5, 3, 'Inception', 'Velice dobrý film a ještě lepší příspěvek o něm.', 0, 1, 1, 'oop-du-04.pdf', '2018-12-20 11:39:32', 2, 3.33333);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (6, 3, 'The Dark Knight', 'Batman jede.', 0, 0, 1, 'oop-du-05.pdf', '2018-12-20 11:41:51', 0, NULL);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (7, 3, 'Green Mile', 'Super popisek, je krátký a shrnuje opravdu vše co je o tomto příspěvku potřeba vědět.', 1, 1, 0, 'oop-du-06.pdf', '2018-12-20 11:42:44', 3, 3.66667);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (8, 3, 'Memento', 'Wow, super film který je třeba sledovat velice pozorně.', 0, 0, 0, 'oop-du-07.pdf', '2018-12-20 11:43:07', 0, NULL);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (9, 3, 'Untouchables', 'Velice vtipný filmek.', 0, 1, 0, 'oop-du-08.pdf', '2018-12-20 11:44:01', 0, NULL);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (10, 3, 'The Prestige', 'Poměrně složité na popsání.', 0, 1, 0, 'oop-du-09.pdf', '2018-12-20 11:44:29', 1, 1);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (11, 3, 'The Shutter Island', 'Tajemný popisek o tajemném filmu, zde je ještě něco napsáno.', 0, 1, 0, 'oop-du-10.pdf', '2018-12-20 11:45:00', 2, 2.5);
INSERT INTO `web-semestralka`.`contribution` (`idContribution`, `idUser`, `movieTitle`, `content`, `public`, `assigned`, `declined`, `fileName`, `addDate`, `ratingsAmount`, `overallRating`) VALUES (12, 3, 'Requiem for a dream', 'Film o drogách, příspěvek popisuje film.', 1, 1, 0, 'oop-du-11.pdf', '2018-12-20 11:45:29', 3, 2.44444);

COMMIT;


-- -----------------------------------------------------
-- Data for table `web-semestralka`.`rating`
-- -----------------------------------------------------
START TRANSACTION;
USE `web-semestralka`;
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (1, 2, 2, 3, 4, 4, 3.66667);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (2, 4, 2, 5, 5, 5, 5);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (3, 5, 2, 3, 2, 4, 3);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (4, 6, 2, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (5, 2, 5, 4, 2, 4, 3.33333);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (6, 4, 5, 3, 5, 2, 3.33333);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (7, 5, 5, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (8, 2, 7, 4, 4, 4, 4);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (9, 4, 7, 4, 5, 2, 3.66667);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (10, 5, 7, 4, 4, 2, 3.33333);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (11, 2, 10, 2, 1, 0, 1);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (12, 4, 10, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (13, 5, 10, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (14, 6, 9, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (15, 7, 9, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (16, 8, 9, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (17, 2, 11, 3, 1, 2, 2);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (18, 4, 11, 3, 4, 2, 3);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (19, 5, 11, NULL, NULL, NULL, NULL);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (20, 2, 12, 3, 2, 2, 2.33333);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (21, 4, 12, 3, 4, 1, 2.66667);
INSERT INTO `web-semestralka`.`rating` (`idRating`, `idUser`, `idContribution`, `theme`, `originality`, `language`, `overall`) VALUES (22, 5, 12, 1, 4, 2, 2.33333);

COMMIT;

