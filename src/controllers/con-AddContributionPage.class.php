<?php

/**
 * Controller that tries to upload the file that the user wants to upload.
 */
class ConAddContributionPage implements IController {

	/** @var AddContributionPage The template page where the user can add the contribution. */
	private $addPage;
	/** @var Database The database of the application. */
	private $database;
	/** @var array Array of errors that can occur during the file uploading. */
	private $errors;

	/**
	 * ConAddContributionPage constructor.
	 */
	public function __construct() {
		require_once("views/view-AddContributionPage.class.php");
		$this->addPage = new AddContributionPage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page based on the user's interaction.
	 */
	public function showResult() {
		$this->addContribution();
		$this->addPage->render();
	}

	/**
	 * Tries to add the contribution to the database and upload the file to the server.
	 */
	private function addContribution() {
		if (isset($_POST["addCntr"])) {
			// In case the directory where the contributions will be stored doesn't exist yet
			if (!file_exists(CONTRIBUTION_DIR)) {
				mkdir(CONTRIBUTION_DIR);
			}

			$valid = true;
			$fileName = $_FILES["file"]["name"];
			// The directory where the contribution file will be stored, the structure will be contributions/USER_LOGIN/
			$targetDir = CONTRIBUTION_DIR . "/" . $this->addPage->getUser()->getID() . "/";
			if (!file_exists($targetDir)) {
				mkdir($targetDir);
			}

			$file = $targetDir . basename($fileName);
			$fileExtension = strtolower(pathinfo($file, PATHINFO_EXTENSION));

			// If the user tries to upload a file with the same name, it will be rejected
			if (file_exists($file)) {
				$valid = false;
				$this->errors["fileExists"] = "Soubor s tímto názvem již existuje.";
			}

			// Checking if the file is in a valid format
			if ($fileExtension != "pdf") {
				$valid = false;
				$this->errors["invalidExtenstion"] = "Zvolený soubor není typu pdf.";
			}

			if ($valid) {
				// Checking if the file was uploaded successfully
				if (move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
					$this->addPage->setSuccessMsg("Soubor s příspěvkem byl úspěšně nahrán,
					 váš příspěvek bude v co nejbilžší době ohodnocen.");

					$this->addToDatabase($fileName);
				}
				else {
					$this->errors["failedUpload"] = "Uložení souboru se nepodařilo.";
				}
			}
			$this->addPage->setErrors($this->errors);
		}
	}

	/**
	 * Adds the information about the contribution to the database.
	 *
	 * @param string $fileName the name of the file that the user added
	 */
	private function addToDatabase($fileName) {
		$movieTitle = htmlspecialchars($_POST["movieTitle"]);
		$abstract = htmlspecialchars($_POST["abstract"]);
		$userID = $this->addPage->getUser()->getID();
		date_default_timezone_set("Europe/Prague");
		$addDate = date("Y-m-d H:i:s");

		$this->database->addContribution($userID, $movieTitle, $abstract, $addDate, $fileName);
	}
}