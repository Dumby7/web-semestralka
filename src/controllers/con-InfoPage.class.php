<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 14.12.2018
 * Time: 20:20
 */

class ConInfoPage implements IController {

	private $infoPage;

	/**
	 * ConInfoPage constructor.
	 */
	public function __construct() {
		require_once("views/view-InfoPage.class.php");
		$this->infoPage = new InfoPage();
	}


	public function showResult() {
		$this->infoPage->render();
	}
}