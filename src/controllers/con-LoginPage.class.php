<?php

/**
 * Controller of the page where the user can log in.
 */
class ConLogPage implements IController {

	/** @var LoginPage The login page. */
	private $logPage;
	/** @var Database The database of the website. */
	private $database;
	/** @var array Errors that occur while trying to log in. */
	private $errors;

	/**
	 * Creates a new controller of the log in page.
	 */
	public function __construct() {
		require_once("views/view-LoginPage.class.php");
		$this->logPage = new LoginPage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page. If the user sends out the form tries to log him in.
	 */
	public function showResult() {
		$this->login();
		$this->logPage->render();
	}

	/**
	 * If the user sends out the form for login, tries to log him in. If the user is in the web application database
	 * he is logged in and redirected to the main page. Else he is informed about his errors.
	 */
	private function login() {
		if (isset($_POST["loginSubmit"])) {
			$this->errors = array();
			$login = htmlspecialchars($_POST["login"]);
			$password = htmlspecialchars($_POST["password"]);

			$hashedPasswordInDB = $this->database->getPassword($login);

			if ($hashedPasswordInDB != null && password_verify($password, $hashedPasswordInDB)) {
				if (!$this->database->isBanned($login)) {
					$this->logPage->getUser()->login($login, $hashedPasswordInDB);
					header("Location: index.php?page=main");
				}
				else {
					$this->errors["ban"] = "Tento účet byl zabanován.";
				}
			}
			else {
				$this->errors["wrongLogin"] = "Zadané přihlašovací jméno nebo heslo je špatné.";
			}

			$this->logPage->setErrors($this->errors);
		}
	}
}