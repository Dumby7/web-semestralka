<?php

/**
 * Controller that logs the user out and shows the user the information about the log out.
 */
class ConLogoutPage implements IController {

	/** @var LogoutPage The template that shows the html code. */
	private $logoutPage;

	/**
	 * ConLogoutPage constructor.
	 */
	public function __construct() {
		require_once("views/view-LogoutPage.class.php");
		$this->logoutPage = new LogoutPage();
	}

	public function showResult() {
		$this->logout();
		$this->logoutPage->render();
	}

	/**
	 * Logs the user out, and sends a message to the template about what way was the user logged out.
	 */
	private function logout() {
		if (isset($_POST["logout"])) {
			$this->logoutPage->setLogout("default");
			$this->logoutPage->getUser()->logout();
		}

		if (isset($_SESSION["logout"]) && $_SESSION["logout"] == "timeout") {
			$this->logoutPage->setLogout("timeout");
			$this->logoutPage->getUser()->logout();
		}
	}
}