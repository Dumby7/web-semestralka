<?php

/**
 * Controller of the main page of the website.
 */
class ConMainPage implements IController {

	/**
	 * @var MainPage The page that this controller controls.
	 */
	private $mainPage;
	/** @var Database The database of the application. */
	private $database;

	/**
	 * Creates a new instance of the controller.
	 */
	public function __construct() {
		require_once("views/view-MainPage.class.php");
		$this->mainPage = new MainPage();
		$this->database = new Database();
	}

	/**
	 * Displays the page.
	 */
	public function showResult() {
		$this->mainPage->setContributions($this->database->getAllContributions());
		$this->mainPage->render();
	}
}