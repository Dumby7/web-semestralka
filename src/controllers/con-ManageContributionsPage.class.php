<?php

/**
 * Controller that shows the result of the template that enables the admin to manage the contributions.
 */
class ConManageContributionsPage implements IController {

	/** @var ManageContributionsPage The page template that this controller controls. */
	private $manageCntrPage;
	/** @var Database The database of the application. */
	private $database;
	/** @var array The errors that occurred during the managing process */
	private $errors;

	/**
	 * ConAssignContributionsPage constructor.
	 */
	public function __construct() {
		require_once("views/view-ManageContributionsPage.class.php");
		$this->manageCntrPage = new ManageContributionsPage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page after the interaction.
	 */
	public function showResult() {
		$this->publish();
		$this->unPublish();
		$this->decline();
		$this->assignContribution();

		$this->manageCntrPage->setContributions($this->database->getAllContributions());
		$this->manageCntrPage->setReviewers($this->database->getReviewers());
		$this->manageCntrPage->setRatings($this->database->getAllRatings());

		$this->manageCntrPage->setErrors($this->errors);

		$this->manageCntrPage->render();
	}

	/**
	 * Tries to assign the contribution to the reviewers. If less than 3 reviewers were selected, sets an error.
	 * Else links each reviewer with the contribution through the database ratings table.
	 */
	private function assignContribution() {
		if (isset($_POST["assign"])) {
			$contrID = $_POST["contrID"];
			if (isset($_POST["reviewersIDs"])) {
				$reviewersIDs = $_POST["reviewersIDs"];

				if (count($reviewersIDs) >= 3) {
					foreach ($reviewersIDs as $reviewersID) {
						$this->database->assignReviewerToContr($reviewersID, $contrID);
					}
					$this->manageCntrPage->setSuccessMsg("Přídání recenzentů k příspěvku " . $contrID . " proběhlo úspěšně.");
					//TODO potreba redirect protoze pokud se neredirectne uzivatel muze refreshnout stranku a data se do databaze ulozi znovu, ovsem nevypise se zprava o uspechu
					header("Location: index.php?page=manageContributions");
				}
				else {
					$this->errors["notEnoughReviewers"] = "Pro přiazení recenzentů k příspěvku číslo " . $contrID . " musíte vybrat alespoň 3 recenzenty!";
				}
			}
			else {
				$this->errors["notEnoughReviewers"] = "Pro přiazení recenzentů k příspěvku číslo " . $contrID . " musíte vybrat alespoň 3 recenzenty!";
			}
		}
	}

	/**
	 * Makes the selected contribution public.
	 */
	private function publish() {
		if (isset($_POST["publish"])) {
			$this->database->publish($_POST["contrID"]);
			$this->manageCntrPage->setSuccessMsg("Příspěvek číslo " . $_POST["contrID"] . " byl zpřístupněn veřejnosti.");
		}
	}

	/**
	 * Makes the selected contribution private.
	 */
	private function unPublish() {
		if (isset($_POST["unPublish"])) {
			$this->database->unPublish($_POST["contrID"]);
			$this->manageCntrPage->setSuccessMsg("Příspěvek číslo " . $_POST["contrID"] . " byl znepřístupněn.");
		}
	}

	private function decline() {
		if (isset($_POST["decline"])) {
			$this->database->decline($_POST["contrID"]);
			$this->manageCntrPage->setSuccessMsg("Příspěvek číslo " . $_POST["contrID"] . " byl zamítnut.");
		}
	}
}