<?php

/**
 * The controller that controls the page where the user views his contributions.
 */
class ConMyContributionsPage implements IController {

	/** @var MyContributionsPage The template page. */
	private $myContrPage;
	/** @var Database The database of the application. */
	private $database;
	/** @var array The errors that can occur. */
	private $errors;

	/**
	 * ConMyContributionsPage constructor.
	 */
	public function __construct() {
		require_once("views/view-MyContributionsPage.class.php");
		$this->myContrPage = new MyContributionsPage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page.
	 */
	public function showResult() {
		$this->delete();
		$this->myContrPage->setErrors($this->errors);
		$this->myContrPage->setContributions($this->database->getContributions($this->myContrPage->getUser()->getID()));
		$this->myContrPage->render();
	}

	/**
	 * Deletes the contribution from the database, and deletes the file from the server.
	 */
	private function delete() {
		if (isset($_POST["delete"])) {
			if (is_file($_POST["filePath"])) {
				if (unlink($_POST["filePath"])) {
					$this->database->deleteContribution($_POST["contrID"]);
					$this->myContrPage->setSuccessMsg("Příspěvek byl úspěšně odstraněn.");
				}
				else {
					$this->errors["unableToDelete"] = "Soubor příspěvku se nepodařilo odstranit.";
				}
			}
		}
	}
}