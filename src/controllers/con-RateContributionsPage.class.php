<?php

/**
 * This class represents the controller that controls the reviewers ratings.
 */
class ConRateContributionsPage implements IController {

	/** @var RateContributionsPage The template that this controller controls. */
	private $rateContrPage;
	/** @var Database The database of the application. */
	private $database;

	/**
	 * ConRateContributionsPage constructor.
	 */
	public function __construct() {
		require_once("views/view-RateContributionsPage.class.php");
		$this->rateContrPage = new RateContributionsPage();
		$this->database = new Database();
	}

	/**
	 * Shows the end result after all the changes from the user.
	 */
	public function showResult() {
		$this->rateContribution();
		// Getting all of the contributions that have been assigned to the current logged reviewer
		$this->rateContrPage->setContributions($this->database->getContributionsToReview($this->rateContrPage->getUser()->getID()));
		$this->rateContrPage->render();
	}

	/**
	 * Tries to rate, or change the rating of a contribution that the reviewer chose to rate.
	 */
	private function rateContribution() {
		if (isset($_POST["rate"]) || isset($_POST["changeRating"])) {
			$contrID = $_POST["contrID"];
			$ratings = array();
			$ratings["theme"] = $_POST["themeRating" . $contrID];
			$ratings["original"] = $_POST["originalRating" . $contrID];
			$ratings["language"] = $_POST["languageRating" . $contrID];

			$overallRating = 0; // The overall rating is an average value of all the ratings of the reviewer
			foreach ($ratings as $rating) {
				$overallRating += $rating;
			}
			$overallRating /= count($ratings);
			$ratings["overall"] = $overallRating;

			$reviewerID = $this->rateContrPage->getUser()->getID(); // The user id is in this case the id of the reviewer

			// The user is rating the contribution for the first time, adds a new rating to the database
			if (isset($_POST["rate"])) {
				$this->database->addRating($contrID, $ratings, $reviewerID);

				$this->rateContrPage->setSuccessMsg("Příspěvek úspěšně ohodnocen.");
				//TODO: redirect protože pokud uživatel přidá hodnocení, a pak refreshne stránku(pošle formulář znovu), tak se počet hodnocení příspěvku zvětší znovu
				header("Location: index.php?page=rateContributions");
			}

			// The user is changing the old rating to a new one, updating the old one to the current one
			if (isset($_POST["changeRating"])) {
				$this->database->changeRating($contrID, $ratings, $reviewerID);

				$this->rateContrPage->setSuccessMsg("Hodnocení příspěvku bylo úspěšně změněno.");
			}

			// Updates the total average rating of the contribution, based on all of the reviewers ratings
			$this->database->updateOverallRating($contrID);
		}
	}
}