<?php

/**
 * Controller class of the registration page. Adds a new user to the database, shows the result of the page.
 */
class ConRegPage implements IController {

	/** @var RegistrationPage The registration page that the user uses for registration. */
	private $regPage;
	/** @var Database The database of the web aplication. */
	private $database;
	/** @var array The array of errors that occurred during the registration process. */
	private $errors;

	/**
	 * Creates a new controller for the RegistrationPage.
	 */
	public function __construct() {
		require_once("views/view-RegistrationPage.class.php");
		$this->regPage = new RegistrationPage();
		$this->database = new Database();
	}

	/**
	 * Shows the page to the user. If  the user sends out the registration form, tries to add a new user to the database.
	 */
	public function showResult() {
		$this->register();
		$this->regPage->render();
	}

	/**
	 * If the user send out the registration form, tries to add a new user to the database.
	 * If the user's input goes through the validation creates a new user, otherwise sends the errors
	 * to the registration page. After successful registration the user is immediately logged in
	 * and redirected to the main page.
	 */
	private function register() {
		if (isset($_POST["registration"])) {
			$this->errors = array();
			$name = htmlspecialchars($_POST["name"]);
			$login = htmlspecialchars($_POST["login"]);
			$password = htmlspecialchars($_POST["password"]);
			$passwordCheck = htmlspecialchars($_POST["passwordCheck"]);
			$email = htmlspecialchars($_POST["email"]);

			if ($this->verifyInput($login, $email, $password,$passwordCheck)) {
				$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

				$this->database->createNewUser($name, $login, $hashedPassword, $email);
				// logging the user in on registration
				$this->regPage->getUser()->login($login, $hashedPassword);
				// relocating the user to the main page after successful registration
				header("Location: index.php?page=main");
			}
			else {
				$this->regPage->setErrors($this->errors);
			}
		}
	}

	/**
	 * Verifies the input of the user in the registration form. Using {@link verifyLogin}, {@link verifyEmail},
	 * {@link verifyPassword}.
	 *
	 * @param string $login the login of the user
	 * @param string $email the email of the user
	 * @param string $password the password of the user
	 * @param string $passwordCheck the password confirmation of the user
	 * @return bool true if all of the input was valid, false otherwise
	 */
	private function verifyInput($login, $email, $password, $passwordCheck) {
		$verified = true;

		if (!$this->verifyLogin($login)) {
			$verified = false;
		}
		if (!$this->verifyEmail($email)) {
			$verified = false;
		}
		if (!$this->verifyPassword($password, $passwordCheck)) {
			$verified = false;
		}

		return $verified;
	}

	/**
	 * Checks if the login is valid. The login has to start with a character, has to be at least 3 characters long
	 * and can only contain characters and digits. Also checks if the login exists in the database.
	 * If either fails error message is stored in the {@link $errors} array.
	 *
	 * @param string $login
	 * @return bool true if the login is accepted false otherwise
	 */
	private function verifyLogin($login) {
		$verified = true;
		if (preg_match("/^[a-zA-Z][a-zA-Z0-9_]{2,32}$/", $login) == 0) {
			$this->errors["usernameInvalid"] = "Uživatelské jméno být alespoň 3 znaky dlouhé, začínat písmenem
			a smí obsahovat pouze číslice a písmena.";
			$verified = false;
		}

		if ($this->database->existsLogin($login)) {
			$this->errors["usernameExists"] = "Uživatel s tímto přihlašovacím jménem již existuje.";
			$verified = false;
		}

		return $verified;
	}

	/**
	 * Validates the e-mail address that the user wants to register with.
	 * First checks if the e-mail is in a valid e-mail format, and then checks if the e-mail exists in the user
	 * database. If any of these fail an error message is stored in the {@link $errors} array.
	 *
	 * @param $email string the email to be verified
	 * @return bool true if the email is valid and doesn't already exist, false otherwise
	 */
	private function verifyEmail($email) {
		$verified = true;
		// Checking if the email is in a valid format
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->errors["emailInvalid"] = "Nevalidní e-mailová adresa.";
			$verified = false;
		}

		// Checking for existing email
		if ($this->database->existsEmail($email)) {
			$this->errors["emailExists"] = "Uživatel s tímto e-mailem již existuje.";
			$verified = false;
		}

		return $verified;
	}

	/**
	 * Validates the password. The first password has to be the same as the second password.
	 * The password has to be at least 5 characters long. If either fails an error message is stored in the
	 * {@link $errors} array.
	 *
	 * @param string $password the password to be validated
	 * @param string $passwordCheck the confirmation password of the password
	 * @return bool true if the passwords match and are at least 5 characters long, false otherwise
	 */
	private function verifyPassword($password, $passwordCheck) {
		$verified = true;
		if ($password != $passwordCheck) {
			$this->errors["passDiff"] = "Hesla se neshodují.";
			$verified = false;
		}

		if (strlen($password) < 5) {
			$this->errors["passLength"] = "Heslo musí být alespoň 5 znaků dlouhé.";
			$verified = false;
		}

		return $verified;
	}
}