<?php

/**
 * Controller that shows the result of the template that enables the admin to manage users.
 */
class ConUserManagePage implements IController {

	/** @var UserManagePage The template where the admin can manage the users. */
	private $userManagePage;
	/** @var Database The database of the application. */
	private $database;


	/**
	 * ConUserManagePage constructor.
	 */
	public function __construct() {
		require_once("views/view-UserManagePage.class.php");
		$this->userManagePage = new UserManagePage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page after the interaction.
	 */
	public function showResult() {
		$this->manageUsers();
		$this->userManagePage->setUsers($this->database->getUsersInfo());
		$this->userManagePage->setAuths($this->database->getAllAuth());
		$this->userManagePage->render();
	}

	/**
	 * Updates the information of the selected user, based on the action that was made.
	 */
	private function manageUsers() {
		if (isset($_POST["userChanges"]) && isset($_POST["idUser"])) {
			$name = $_POST["name"];
			$login = $_POST["login"];
			$hashedPassword = $this->database->getPassword($login);
			$email = $_POST["email"];
			$idAuthorization = $_POST["idAuthorization"];

			$this->database->updateUserInfo($name, $login, $hashedPassword, $email, $idAuthorization);
			$this->userManagePage->setSuccessMsg("Byla provedena změna informací uživatle s ID číslo:&nbsp;" . $_POST["idUser"] . ".");
		}
		elseif (isset($_POST["ban"]) && isset($_POST["idUser"])) {
			$login = $_POST["login"];

			$this->database->ban($login);

			$this->userManagePage->setSuccessMsg("Uživatel s ID " . $_POST["idUser"] . " byl zabanován.");
		}
		elseif (isset($_POST["unban"]) && isset($_POST["idUser"])) {
			$login = $_POST["login"];

			$this->database->unban($login);

			$this->userManagePage->setSuccessMsg("Uživatel s ID " . $_POST["idUser"] . " byl odbanován.");
		}
	}
}