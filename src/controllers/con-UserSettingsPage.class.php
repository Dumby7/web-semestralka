<?php

/**
 * Controller that tries to change the user settings if the user tries to change them.
 */
class ConUserSettingsPage implements IController {
	/** @var UserSettingsPage The page where the user changes the settings. */
	private $usrSettingPage;
	/** @var Database Database of the web application. */
	private $database;
	/** @var array Errors that occur during the updating process. */
	private $errors;

	/**
	 * Creates a new controller for the update settings page.
	 */
	public function __construct() {
		require_once("views/view-Page.class.php");
		require_once("views/view-UserSettingsPage.class.php");
		$this->usrSettingPage = new UserSettingsPage();
		$this->database = new Database();
	}

	/**
	 * Shows the result of the page.
	 */
	public function showResult() {
		$this->updateUserSettings();
		$this->usrSettingPage->render();
	}

	/**
	 * Updates the user settings if the user send out the form. Validates the input and if it is incorrect
	 * sends the message to the view page. If the input is valid updates the information about the user in the database
	 * and sends the view page a message that the update was successful.
	 */
	private function updateUserSettings() {
		if (isset($_POST["updateSettings"])) {
			// Checking if the user input a new value, if not sets the new value to the current one
			$newName = htmlspecialchars($_POST["name"] != "" ? $_POST["name"] : $this->usrSettingPage->getUser()->getName());
			$newPass = htmlspecialchars($_POST["newPass"] != "" ? $_POST["newPass"] : $this->usrSettingPage->getUser()->getPassword());
			$confPass = htmlspecialchars($_POST["confPass"] != "" ? $_POST["confPass"] : $this->usrSettingPage->getUser()->getPassword());
			$newEmail = htmlspecialchars($_POST["email"] != "" ? $_POST["email"] : $this->usrSettingPage->getUser()->getEmail());

			$newPassHash = null;

			// If nothing was input in the password box, than there is no point hashing, else hashing the password
			if ($_POST["newPass"] == "") {
				$newPassHash = $this->usrSettingPage->getUser()->getPassword();
			}
			else {
				$newPassHash = password_hash($_POST["newPass"], PASSWORD_DEFAULT);
			}

			// If the user input was valid, update his info and send out success msg, else send out errors
			if ($this->verifyInput($newEmail, $newPass, $confPass)) {
				$login = $this->usrSettingPage->getUser()->getLogin();
				// Getting the id of the authorization of the user based on the authorization name
				$idAuth = $this->database->getAuthID($this->usrSettingPage->getUser()->getAuthorizationName());

				$this->database->updateUserInfo($newName, $login, $newPassHash, $newEmail, $idAuth);
				$this->usrSettingPage->getUser()->login($login, $newPassHash);
				$this->usrSettingPage->setSuccessMsg("Změna osobních údajů proběhla v&nbsp;pořádku.");
			}
			else {
				$this->usrSettingPage->setErrors($this->errors);
			}
		}
	}

	/**
	 * Verifies the new input from the user. Using {@link verifyEmail}, {@link verifyPassword}.
	 *
	 * @param string $email the email that the user input
	 * @param string $password the password that the user input
	 * @param string $passwordCheck the password confirmation that the user input
	 * @return bool true if all the input is valid, false otherwise
	 */
	private function verifyInput($email, $password, $passwordCheck) {
		$verified = true;

		if (!$this->verifyEmail($email)) {
			$verified = false;
		}
		if (!$this->verifyPasswords($password, $passwordCheck)) {
			$verified = false;
		}

		return $verified;
	}

	/**
	 * If the user input a new email, verifies it.
	 *
	 * @param string $email the email to be verified
	 * @return bool true if the email is valid, false otherwise
	 */
	private function verifyEmail($email) {
		$verified = true;
		// If the user left the email the same, there is no point in verifying it because it has to already be valid
		if ($email != $this->usrSettingPage->getUser()->getEmail()) {
			// Checking if the string email is a valid email address
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$this->errors["emailNotValid"] = "Nevalidní e-mailová adresa.";
				$verified = false;
			}

			// Checking for existing email
			if ($this->database->existsEmail($email)) {
				$this->errors["emailExists"] = "Uživatel s tímto e-mailem již existuje.";
				$verified = false;
			}
		}

		return $verified;
	}

	/**
	 * If the user input a new password, verifies it.
	 *
	 * @param string $password the password that the user wants to input
	 * @param string $passwordCheck confirmation of the password
	 * @return bool true if the password is valid, false otherwise
	 */
	private function verifyPasswords($password, $passwordCheck) {
		$verified = true;
		if ($password != $this->usrSettingPage->getUser()->getPassword()) {
			if ($password != $passwordCheck) {
				$this->errors["passDiff"] = "Hesla se neshodují.";
				$verified = false;
			}

			if (strlen($password) < 5) {
				$this->errors["passLength"] = "Heslo musí být alespoň 5 znaků dlouhé.";
				$verified = false;
			}
		}
		return $verified;
	}
}