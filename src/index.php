<?php

// Loading the settings of the web application
require_once("settings.php");
require_once("controllers/con-IController.interface.php");
require_once("views/view-Page.class.php");

if (isset($_GET["page"]) && array_key_exists($_GET["page"], $webPages)) {
	$page = $_GET["page"];
}
else {
	$page = DEFAULT_PAGE;
}

require("controllers/" . $webPages[$page]['file']);

$ControllerObject = $webPages[$page]["object"];

$controller = new $ControllerObject;

echo $controller->showResult();