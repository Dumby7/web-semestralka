<?php

/**
 * Class that works with the database of the web.
 */
class Database {

	/** @var PDO Connection to the database. */
	private $pdo;

	/**
	 * Database constructor.
	 */
	public function __construct() {
		$this->pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
		$this->pdo->query("SET NAMES utf8");
	}

	/**
	 * Gets information about all of the users in the database. The information is: id of the user, login, email,
	 * name, the ban information, id of the users authorization, name of the authorization. The info doesn't contain
	 * the users passwords, if a password is for some reason needed use the getPassword function.
	 *
	 * @return array info about all the users in the database
	 */
	public function getUsersInfo() {
		$statement = $this->pdo->prepare(
			"SELECT u.idUser AS idUser, u.login AS login, u.email AS email, u.name AS name, u.ban AS ban, a.idAuthorization AS idAuthorization, a.name AS authorizationName
					   FROM " . TAB_USERS . " u, " . TAB_AUTH . " a 
					   WHERE u.idAuthorization = a.idAuthorization ORDER BY u.idUser");
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Creates a new user in the database.
	 *
	 * @param string $name the name that the user chose
	 * @param string $login the login that the user will use to log into the website
	 * @param string $password the password that the user will use to log into the website
	 * @param string $email the email that the user input
	 */
	public function createNewUser($name, $login, $password, $email) {
		$statement = $this->pdo->prepare("INSERT INTO " . TAB_USERS . " (name, login, password, email, ban, idAuthorization)
		 								VALUES(:name, :login, :password, :email, :ban, :idAuthorization)");

		// Finding the id of the Authorization for Author
		$idAuthorizationArr = $this->pdo->query("SELECT idAuthorization FROM " . TAB_AUTH . " WHERE name = 'Author'")->fetchAll();
		$idAuthorization = $idAuthorizationArr[0]["idAuthorization"];
		$ban = 0;

		$statement->bindParam(":name", $name);
		$statement->bindParam(":login", $login);
		$statement->bindParam(":password", $password);
		$statement->bindParam(":email", $email);
		$statement->bindParam(":ban", $ban);
		$statement->bindParam(":idAuthorization", $idAuthorization);

		$statement->execute();
	}

	/**
	 * Checks if a user already exists with the specified login.
	 *
	 * @param string $login the login that is being looked for
	 * @return bool true if the user with this login already exists, false otherwise
	 */
	public function existsLogin($login) {
		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_USERS . " WHERE login = :login");
		$statement->bindParam(":login", $login);
		$statement->execute();
		$result = $statement->fetchAll();
		if ($result != null) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if a user already exists with the specified email.
	 *
	 * @param string $email the email that is being looked for
	 * @return bool true if the user with this email already exists, false otherwise
	 */
	public function existsEmail($email) {
		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_USERS . " WHERE email = :email");
		$statement->bindParam(":email", $email);
		$statement->execute();
		$result = $statement->fetchAll();
		if ($result != null) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the hashed password of the user that is stored in the database.
	 *
	 * @param string $login
	 * @return string
	 */
	public function getPassword($login) {
		$statement = $this->pdo->prepare("SELECT password FROM " . TAB_USERS . " WHERE login = :login");
		$statement->bindParam(":login", $login);
		$statement->execute();
		$res = $statement->fetchAll(PDO::FETCH_ASSOC);

		if (array_key_exists(0, $res)) {
			return $res[0]["password"];
		}
		return null;
	}

	/**
	 * Returns the id of the user with the specified login.
	 *
	 * @param string $login the login of the user
	 * @return string|null the id or null
	 */
	public function getUserID($login) {
		$statement = $this->pdo->prepare("SELECT idUser FROM " . TAB_USERS . " WHERE login = :login");
		$statement->bindParam(":login", $login);
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		if (array_key_exists(0, $result)) {
			return $result[0]["idUser"];
		}
		return null;
	}

	/**
	 * Checks if the user with the specified login is banned or not. If the value in the ban column is equal to 0
	 * returns false, true otherwise. If the login was not found in the database returns true also.
	 *
	 * @param string $login the login of the user that is being checked
	 * @return bool true if the user is banned, false otherwise
	 */
	public function isBanned($login) {
		$statement = $this->pdo->prepare("SELECT ban FROM " . TAB_USERS . " WHERE login = :login");
		$statement->bindParam(":login", $login);
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		if ($result != null) {
			return $result[0]["ban"] == 1;
		}

		return true;
	}

	/**
	 * Returns the name of the user with the login and password. The function expects that the user actually exists.
	 *
	 * @param string $login login of the user
	 * @param string $password password of the user
	 * @return string name of the user
	 */
	public function getName($login, $password) {
		$statement = $this->pdo->prepare("SELECT name FROM " . TAB_USERS . " WHERE login = :login AND password = :password");
		$statement->bindParam(":login", $login);
		$statement->bindParam(":password", $password);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]["name"];
	}

	/**
	 * Returns the email of the user with the login and password. The function expects that the user actually exists.
	 *
	 * @param string $login login of the user
	 * @param string $password password of the user
	 * @return string email of the user
	 */
	public function getEmail($login, $password) {
		$statement = $this->pdo->prepare("SELECT email FROM " . TAB_USERS . " WHERE login = :login AND password = :password");
		$statement->bindParam(":login", $login);
		$statement->bindParam(":password", $password);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]["email"];
	}

	/**
	 * Returns the information about the authorization of the user with the specified login and password.
	 * The user has to exist in the database.
	 *
	 * @param string $login login of the user
	 * @param string $password password of the user
	 * @return string name of the user
	 */
	public function getAuthorization($login, $password) {
		$statement = $this->pdo->prepare("SELECT auth.name, auth.value FROM " . TAB_USERS . " user, " . TAB_AUTH . " auth  
													WHERE user.idAuthorization = auth.idAuthorization 
													AND user.login = :login AND user.password = :password");
		$statement->bindParam(":login", $login);
		$statement->bindParam(":password", $password);
		$statement->execute();
		$res = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $res[0];
	}

	/**
	 * Returns the id of the specified authorization based on the name.
	 *
	 * @param string $authorizationName the name of the authorization whose id is being returned.
	 * @return string the id of the authorization
	 */
	public function getAuthID($authorizationName) {
		$statement = $this->pdo->prepare("SELECT idAuthorization FROM " . TAB_AUTH . " WHERE name = :authorizationName");
		$statement->bindParam(":authorizationName", $authorizationName);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]["idAuthorization"];
	}

	/**
	 * Updates the user's information. The information is changed based of the login of the user. That means that the
	 * login cannot be changed.
	 *
	 * @param string $name the new name of the user
	 * @param string $login the login of the user that is used to identify which user info is to be updated
	 * @param string $password the new password of the user
	 * @param string $email the new email of the user
	 * @param string|int $idAuthorization the new id of the authorization of the user
	 */
	public function updateUserInfo($name, $login, $password, $email, $idAuthorization) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_USERS . " SET name = :name, password = :password, email = :email,
		 						idAuthorization = :idAuthorization WHERE login = :login");
		$statement->bindParam(":name", $name);
		$statement->bindParam(":password", $password);
		$statement->bindParam(":email", $email);
		$statement->bindParam(":idAuthorization", $idAuthorization);
		$statement->bindParam(":login", $login);

		$statement->execute();
	}

	/**
	 * Gets all of the info about the authorizations in the database.
	 *
	 * @return array info about the authorizations
	 */
	public function getAllAuth() {
		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_AUTH);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Bans the user with the specified login.(Sets the value of the ban column to 1.)
	 *
	 * @param string $login login of the user that is being banned
	 */
	public function ban($login) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_USERS . " SET ban = :ban WHERE login = :login");
		$ban = 1;
		$statement->bindParam(":ban", $ban);
		$statement->bindParam(":login", $login);

		$statement->execute();
	}

	/**
	 * Unbans the user with the specified login.(Sets the value of the ban column to 0.)
	 *
	 * @param string $login login of the user that is being unbanned
	 */
	public function unban($login) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_USERS . " SET ban = :ban WHERE login = :login");
		$ban = 0;
		$statement->bindParam(":ban", $ban);
		$statement->bindParam(":login", $login);

		$statement->execute();
	}

	/**
	 * Adds the information about the contribution that the user added to the database.
	 *
	 * @param int $idUser the id of the user that added the contribution
	 * @param string $movieTitle the title of the movie that the contribution is about
	 * @param string $content short commentary that describes the contribution
	 * @param string $addDate date when the user added the contribution in format: Y-m-d H:i:s
	 * @param string $fileName the name of the file of the contribution
	 */
	public function addContribution($idUser, $movieTitle, $content, $addDate, $fileName) {
		$statement = $this->pdo->prepare("INSERT INTO " . TAB_CONTRIBUTIONS . " 
										(idUser, movieTitle, content, addDate, public, assigned, declined, fileName, ratingsAmount) 
										VALUES(:idUser, :movieTitle, :content, :addDate, :public, :assigned, :declined, :fileName, :ratingsAmount)");
		$public = 0;
		$assigned = 0;
		$ratingsAmount = 0;
		$declined = 0;
		$statement->bindParam(":idUser", $idUser);
		$statement->bindParam(":movieTitle", $movieTitle);
		$statement->bindParam(":content", $content);
		$statement->bindParam(":addDate", $addDate);
		$statement->bindParam(":public", $public);
		$statement->bindParam(":assigned", $assigned);
		$statement->bindParam(":declined", $declined);
		$statement->bindParam(":fileName", $fileName);
		$statement->bindParam(":ratingsAmount", $ratingsAmount);

		$statement->execute();
	}

	/**
	 * Gets the information about the contributions and their authors from the database. The contributions are ordered
	 * by the time they were added from the oldest to the newest.
	 *
	 * @return array the information about the contributions
	 */
	public function getAllContributions() {
		$statement = $this->pdo->prepare("SELECT c.*, u.* FROM " . TAB_CONTRIBUTIONS . " c, " . TAB_USERS . " u 
								  	  	 WHERE  c.idUser = u.idUser ORDER BY c.addDate");
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Gets all the users from the database that have the authorization name Reviewer.
	 *
	 * @return array all the reviewers in the database
	 */
	public function getReviewers() {
		$statement = $this->pdo->prepare("SELECT idAuthorization FROM " . TAB_AUTH . " WHERE name = 'Reviewer'");
		$statement->execute();
		$idAuthorization = $statement->fetchAll(PDO::FETCH_ASSOC)[0]["idAuthorization"];

		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_USERS . " WHERE idAuthorization = '$idAuthorization'");
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Returns all the information about each rating from the rating table in the database.
	 *
	 * @return array the information
	 */
	public function getAllRatings() {
		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_RATINGS);
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Links the reviewer to the contribution via the rating table.
	 *
	 * @param string $reviewerID id of the reviewer
	 * @param string $contrID if of the contribution
	 */
	public function assignReviewerToContr($reviewerID, $contrID) {
		$statement = $this->pdo->prepare("INSERT INTO " . TAB_RATINGS . " (idUser, idContribution) VALUES(:idReviewer, :idContribution)");
		$statement->bindParam(":idReviewer", $reviewerID);
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();

		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET assigned = 1 WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Sets the value of the public attribute to 1 for the given contribution.
	 *
	 * @param string $contrID the id of the contribution that will be public
	 */
	public function publish($contrID) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET public = 1 WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Sets the  value of the public attribute to 0 for the given contribution.
	 *
	 * @param string $contrID the id of the contribution that will be private
	 */
	public function unPublish($contrID) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET public = 0 WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Sets the value of the declined attribute to 1, and the public attribute to 0. Making it declined and not public.
	 *
	 * @param int|string $contrID id of the contribution that is to be declined
	 */
	public function decline($contrID) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET declined = 1, public = 0 WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Deletes the information about the contribution from the rating table and from the contribution table in the database.
	 *
	 * @param string $contrID the contribution id that is to be deleted
	 */
	public function deleteContribution($contrID) {
		$statement = $this->pdo->prepare("DELETE FROM " . TAB_RATINGS . " WHERE  idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();

		$statement = $this->pdo->prepare("DELETE FROM " . TAB_CONTRIBUTIONS . " WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Returns all of the contributions info that the reviewer ID has been assigned to.
	 * Only returns contributions that have not been published yet and the ones that have not been declined.
	 *
	 * @param string|int $reviewerID the id of the reviewer that the contributions info is connected to
	 * @return array information about the contributions
	 */
	public function getContributionsToReview($reviewerID) {
		$statement = $this->pdo->prepare("SELECT r.*, c.* FROM " . TAB_RATINGS . " r, " . TAB_CONTRIBUTIONS . " c 
										WHERE r.idUser = :idReviewer AND r.idContribution = c.idContribution AND c.public = 0 
										AND declined = 0 ORDER BY c.addDate");
		$statement->bindParam(":idReviewer", $reviewerID);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * Adds new ratings that the reviewer chose to the contribution. Also increments the total amount of ratings of the
	 * contribution.
	 *
	 * @param string|int $contrID the id of the contribution that is being rated
	 * @param array $ratings the array of all the reviewers ratings
	 * @param string|int $reviewerID the id of the reviewer that wants to assign a new rating to the contribution
	 */
	public function addRating($contrID, array $ratings, $reviewerID) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_RATINGS . " 
										SET theme = :theme, originality = :originality, language = :language, overall = :overall
 										WHERE idContribution = :idContribution AND idUser = :idReviewer");

		$statement->bindParam(":theme", $ratings["theme"]);
		$statement->bindParam(":originality", $ratings["original"]);
		$statement->bindParam(":language", $ratings["language"]);
		$statement->bindParam(":overall", $ratings["overall"]);
		$statement->bindParam(":idContribution", $contrID);
		$statement->bindParam(":idReviewer", $reviewerID);

		$statement->execute();

		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET ratingsAmount= ratingsAmount + 1
		 								WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);

		$statement->execute();
	}

	/**
	 * Changes the current rating of a contribution from the reviewer.
	 *
	 * @param string|int $contrID the contribution which rating is being changed
	 * @param array $ratings the new ratings that are being assigned to the contribution
	 * @param string|int $reviewerID the id of the reviewer that is changing the ratings
	 */
	public function changeRating($contrID, array $ratings, $reviewerID) {
		$statement = $this->pdo->prepare("UPDATE " . TAB_RATINGS . " 
										SET theme = :theme, originality = :originality, language = :language, overall = :overall
 										WHERE idContribution = :idContribution AND idUser = :idReviewer");

		$statement->bindParam(":theme", $ratings["theme"]);
		$statement->bindParam(":originality", $ratings["original"]);
		$statement->bindParam(":language", $ratings["language"]);
		$statement->bindParam(":overall", $ratings["overall"]);
		$statement->bindParam(":idContribution", $contrID);
		$statement->bindParam(":idReviewer", $reviewerID);

		$statement->execute();
	}

	/**
	 * Updates the total overall rating for the contribution. The overall rating is calculated as an average from
	 * all of the reviewers ratings that have rated this contribution.
	 *
	 * @param string|int $contrID the id of the contribution that the rating is being updated
	 */
	public function updateOverallRating($contrID) {
		$statement = $this->pdo->prepare("SELECT overall FROM " . TAB_RATINGS . " 
										WHERE idContribution = :idContribution");
		$statement->bindParam("idContribution", $contrID);
		$statement->execute();

		$ratings = $statement->fetchAll(PDO::FETCH_ASSOC);

		$overallRating = 0;
		$ratingAmount = 0;
		foreach ($ratings as $rating) {
			foreach ($rating as $value) {
				if ($value != null) {
					$ratingAmount++;
					$overallRating += $value;
				}
			}
		}
		$overallRating /= $ratingAmount;

		$statement = $this->pdo->prepare("UPDATE " . TAB_CONTRIBUTIONS . " SET overallRating = '$overallRating' WHERE idContribution = :idContribution");
		$statement->bindParam(":idContribution", $contrID);
		$statement->execute();
	}

	public function getContributions($userID) {
		$statement = $this->pdo->prepare("SELECT * FROM " . TAB_CONTRIBUTIONS . " WHERE idUser = :idUser");
		$statement->bindParam(":idUser", $userID);
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
}