<?php

/**
 * This class represents the user of the application.
 */
class User {

	/** @var Database The database of the application. */
	private $database;

	/**
	 * Starts the session of the user.
	 */
	public function __construct() {
		session_start();
		$this->database = new Database();
	}

	/**
	 * Logs the user into the website. Stores the information in the super global variable _SESSION["user"].
	 *
	 * @param string $login the login of the user
	 * @param string $password the password of the user
	 */
	public function login($login, $password) {
		$name = $this->database->getName($login, $password);
		$email = $this->database->getEmail($login, $password);

		$ID = $this->database->getUserID($login);
		$authorization = $this->database->getAuthorization($login, $password);
		$authorizationName = $authorization["name"];
		$authValue = $authorization["value"];
		$banned = $this->database->isBanned($login);

		$_SESSION["user"] = array("ID" => $ID, "name" => $name, "login" => $login, "password" => $password,
			"email" => $email, "authName" => $authorizationName, "authValue" => $authValue, "banned" => $banned);
	}

	/**
	 * Logs out the user. Unsets the session.
	 */
	public function logout() {
		if ($this->isLoggedIn()) {
			session_unset();
		}
	}

	/**
	 * Checks if the user has logged in.
	 *
	 * @return bool true if the user is logged in, false otherwise
	 */
	public function isLoggedIn() {
		return isset($_SESSION["user"]);
	}

	public function getID() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["ID"];
		}
		return null;
	}

	/**
	 * Returns the login of the user if he is logged in, null otherwise.
	 *
	 * @return string he login of the user
	 */
	public function getLogin() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["login"];
		}
		return null;
	}

	/**
	 * Returns the password of the user if he is logged in, null otherwise.
	 *
	 * @return string he password of the user
	 */
	public function getPassword() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["password"];
		}
		return null;
	}

	/**
	 * Returns the name of the user if he is logged in, null otherwise.
	 *
	 * @return string the name of the user
	 */
	public function getName() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["name"];
		}
		return null;
	}

	/**
	 * Returns the email of the user if he is logged in, null otherwise.
	 *
	 * @return string he email of the user
	 */
	public function getEmail() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["email"];
		}
		return null;
	}

	/**
	 * Returns the name of the authorization of the user if he is logged in, null otherwise.
	 *
	 * @return string the authorization name of the user
	 */
	public function getAuthorizationName() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["authName"];
		}
		return null;
	}

	/**
	 * Returns the value of the authorization of the user if he is logged in, null otherwise.
	 *
	 * @return string the value of the authorization of the user
	 */
	public function getAuthValue() {
		if ($this->isLoggedIn()) {
			return $_SESSION["user"]["authValue"];
		}
		return null;
	}

	/**
	 * Returns true if the user is banned, false otherwise.
	 *
	 * @return bool true if banned, false if not
	 */
	public function isBanned() {
		return $_SESSION["user"]["banned"];
	}
}