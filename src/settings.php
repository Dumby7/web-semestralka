<?php
// Settings of the whole web application

// Including the model classes on a "global" scale
require_once("models/mod-User.class.php");
require_once("models/mod-Database.class.php");

// The database that the application uses
define("DB_SERVER", "localhost");
define("DB_NAME", "web-semestralka");
define("DB_USER", "root");
define("DB_PASS", "");

/** The table where the users information is stored. */
define("TAB_USERS", "user");
/** The table of authorizations of the users. */
define("TAB_AUTH", "authorization");
/** The table of ratings for the contributions given by the reviewers. */
define("TAB_RATINGS", "rating");
/** The table of contributions that the users put on the web. */
define("TAB_CONTRIBUTIONS", "contribution");

/** The directory where all the contributions from users will be stored. */
define("CONTRIBUTION_DIR", "../contributions");

/** All the available webpages of the application. */
global $webPages;

/** The default page of this web application. */
const DEFAULT_PAGE = "main";

// Adding the pages of this application
const PAGES = array(
	DEFAULT_PAGE => array("file" => "con-MainPage.class.php", "object" => "ConMainPage", "title" => "Úvodní stránka",
		"nav" => "Úvod"),
	"info" => array("file" => "con-InfoPage.class.php", "object" => "ConInfoPage", "title" => "Informace",
		"nav" => "Informace")
);

const GET_IN_PAGES = array(
	"log" => array("file" => "con-LoginPage.class.php", "object" => "ConLogPage", "title" => "Přihlášení",
		"nav" => "Přihlásit"),
	"reg" => array("file" => "con-RegistrationPage.class.php", "object" => "ConRegPage", "title" => "Registrace",
		"nav" => "Registrace")
);

const USER_PAGES = array(
	"usermanage" => array("file" => "con-UserManagePage.class.php", "object" => "ConUserManagePage", "title" => "Správa uživatelů",
		"nav" => "Spravovat uživatele"),
	"usersettings" => array("file" => "con-UserSettingsPage.class.php", "object" => "ConUserSettingsPage", "title" => "Nastavení údajů",
		"nav" => "Nastavení")
);

const CONTRIBUTION_PAGES = array(
	"myContributions" => array("file" => "con-MyContributionsPage.class.php", "object" => "ConMyContributionsPage", "title" => "Moje příspěvky",
		"nav" => "Moje příspěvky"),
	"addContribution" => array("file" => "con-AddContributionPage.class.php", "object" => "ConAddContributionPage", "title" => "Přidání příspěvku",
		"nav" => "Přidat příspěvek"),
	"rateContributions" => array("file" => "con-RateContributionsPage.class.php", "object" => "ConRateContributionsPage", "title" => "Hodnocení příspěvků",
		"nav" => "Hodnotit příspěvky"),
	"manageContributions" => array("file" => "con-ManageContributionsPage.class.php", "object" => "ConManageContributionsPage", "title" => "Správa příspěvků",
		"nav" => "Správa příspěvků")
);

const LOGOUT = array(
	"logout" => array("file" => "con-LogoutPage.class.php", "object" => "ConLogoutPage", "title" => "Odhlášení")
);

$webPages = array_merge(PAGES, GET_IN_PAGES, USER_PAGES, CONTRIBUTION_PAGES, LOGOUT);