<?php

/**
 * Class that represents the page where the user can add a new contribution.
 */
class AddContributionPage extends Page {

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages['addContribution']['title']);

		$this->getMainMenu();
		if ($this->user->isLoggedIn()) {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center form-box">
					<?php
					$this->renderErrors();
					$this->renderSuccessMsg();
					?>
					<form class="col-10 col-md-5" autocomplete="off" method="post" enctype="multipart/form-data">
						<h2>Přidání příspěvku</h2>

						<div class="form-group text-box">
							<label for="movieTitle">Název filmu
								<input required id="movieTitle" type="text" name="movieTitle"
								       value="<?php echo @$_POST['movieTitle'] ?>">
							</label>
						</div>

						<div class="form-group">
							<label for="abstract" class="text-box">Abstrakt
								<textarea required id="abstract" name="abstract" class="form-control"
								          rows="4"
								          placeholder="Zde krátce popište o čem je váš příspěvek..."></textarea>
							</label>
						</div>

						<div class="form-group text-box">
							<label>Soubor příspěvku</label><br>

							<input required id="file" class="form-control-file inputfile" accept=".pdf" type="file"
							       name="file">
							<label for="file" class="customUpload"><i
										class="fas fa-file-upload"></i><span>Vybrat soubor</span></label>
						</div>

						<input class="submit-btn" type="submit" name="addCntr" value="Přidat příspěvek">

					</form>
				</div>
			</div>
			<?php
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>K zobrazení této stránky musíte být přihlášen.</h3>
				</div>
			</div>
			<?php
		}
		?>
		<!--Script that changes the text of the file choosing button to the file name.-->
		<script>
            const inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input) {
                const label = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener('change', function (e) {
                    const fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });
		</script>
		<?php
		$this->getHTMLFooter();
	}

}