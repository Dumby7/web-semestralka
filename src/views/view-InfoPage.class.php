<?php

/**
 * Page where the user can see the information that he might need.
 */
class InfoPage extends Page {

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages["info"]["title"]);

		$this->getMainMenu();
		?>
		<div class="container justify-content-center col-12 col-md-4">
			<div class="info">
				<div>
					<div class="justify-content-center text-center" style="margin-bottom: 20px">
						<h2>Informace o filmové konferenci</h2>
					</div>
					<?php
					if (!$this->user->isLoggedIn()) {
						?>
						<div>
							<h4>Nepřihlášený uživatel</h4>
							<p>Jako nepřihlášený uživatel si můžete prohlédnout zveřejněné příspěvky na hlavní stránce.
								Pro přidání vlastního příspěvku je potřeba se <a
										href="index.php?page=reg">zaregistrovat</a>,
								čímž automaticky získáváte právo autora.<br>
								<small>Pokud jste již zaregistrovaní: <a href="index.php?page=log">přihlášení</a>.
								</small>
							</p>
							<hr>
						</div>
						<?php
					}
					else {
						?>
						<div>
							<h4>Autorská práva</h4>
							<p>Jako autor můžete přidávat příspěvky o jakémkoliv filmu. Zadává se název filmu, kterého
								se daný
								příspěvek týká a krátké shrnutí, ve kterém příspěvek popíšete. Samotný příspěvek bude
								nahrán
								jako soubor typu <strong>PDF</strong>. Přidané příspěvky budou následě
								administrátory stránek přiřazeny recenzentům k hodnocení. Na základě tohoto hodocení
								bude pak
								rozhodnuto o tom zda bude příspěvek zveřejněn nebo zamítnut. To v jakém stavu se vaše
								příspěvky
								právě nachází můžete sledovat v záložce <i>Moje příspevky</i>. Vaše zveřejněné, nebo
								zamítnuté příspěvky budou v tabulce zvýrazněny.<br>Prosím berte na
								vědomí, že
								hodnocení příspěvků může nějakou dobu trvat.</p>
							<?php
							if ($this->user->getAuthorizationName() == "Author") {
								?>
								<p>Pokud budou vaše příspěvky úspěšné je možné, že vám některý z administrátorů
									dá práva recenzenta, či dokonce administrátora. <strong>Vaše aktuální právo můžete
										vidět vedle vašeho jména v hlavním menu.</strong></p>
								<?php
							}
							?>
						</div>
						<?php
						if ($this->user->getAuthorizationName() == "Reviewer") {
							?>
							<div>
								<hr>
								<h4>Práva recenzentů</h4>
								<p>Jako recenzent, kromě toho že můžete nadále přidávat vlastní příspěvky, bude vaším
									hlavním
									úkolem příspěvky hodnotit. Příspěvky, které budete hodnotit, vám nejprve bude muset
									přiřadit
									administrátor. Vaše hodnocení bude započítáno do celkového hodnocení příspěvku, na
									základě
									kterého se administrátor následně rozhodne jestli příspěvek zveřejní nebo
									zamítne. Dokud příspěvek není zveřejněný, můžete vaše hodnocení změnit.</p>
							</div>
							<?php
						}
						if ($this->user->getAuthorizationName() == "Administrator") {
							?>
							<div>
								<hr>
								<h4>Práva adminstrátorů</h4>
								<p>Hlavním úkolem administártorů je spravovat uživatele a příspěvky. Uživatelům můžete
									měnit práva podle toho jak uznáte za vhodné, případně pokud se bude nějaký uživatel
									snažit narušit svým chováním chod stránky, máte právo ho zabanovat.</p>
								<p>Přidané příspěvky budete přiřazovat recenzentům k hodnocení. Při 3 a více hodnocení
									se
									můžete rozhodnout příspěvek zveřejnit. Pokud se vám zdá, že příspěvek není příliš
									dobrý,
									máte právo ho zamítnout.</p>
							</div>
							<?php
						}
						?>
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<?php
		$this->getHTMLFooter();
	}
}