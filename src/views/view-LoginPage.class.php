<?php

/**
 * The page where the user can log in to the application.
 */
class LoginPage extends Page {

	public function render() {
		$this->getHTMLHeader($this->webPages['log']['title']);

		$this->getMainMenu();

		if (!$this->user->isLoggedIn()) {
			?>

			<!--Form for loging into the website, the form takes 10 columns on the small screens
			and 5 on bigger screens. The content of the form is horizontally centered.-->
			<div class="container">
				<div class="row justify-content-center align-items-center form-box">
					<form id="loginForm" class="col-10 col-md-5" autocomplete="off" method="post">
						<h2>Přihlášení</h2>

						<div class="form-group text-box">
							<label for="login"><i class="fas fa-user"></i></label>
							<input id="login" type="text" placeholder="Přihlašovací jméno" name="login"
							       value="<?php echo @$_POST["login"] ?>">
						</div>

						<div class="form-group text-box">
							<label for="password"><i class="fas fa-unlock"></i></label>
							<input id="password" type="password" placeholder="Heslo" name="password">
						</div>
						<?php
						if (isset($this->errors["ban"])) {
							echo "<small class='form-text warning'>".$this->errors['ban']."</small>";
						}
						if (isset($this->errors["wrongLogin"])) {
							echo "<small class='form-text warning'>".$this->errors['wrongLogin']."</small>";
						}
						?>
						<input class="submit-btn" type="submit" name="loginSubmit" value="Přihlásit">
						<a href="index.php?page=reg"><small id="emailHelp" class="form-text">Nejste registrovaní?</small></a>
					</form>
				</div>
			</div>

			<?php
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>Přihlášený uživatel se nemůže znovu přihlásit.</h3>
				</div>
			</div>
			<?php
		}
		$this->getHTMLFooter();
	}
}