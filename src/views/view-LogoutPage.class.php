<?php

/**
 * Page that shows the confirmation of the logout action.
 */
class LogoutPage extends Page {

	/** @var string How the logout was performed, either the user was timed out, or he willingly logged out. */
	private $logout;

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages["logout"]["title"]);
		$this->getMainMenu();
		if ($this->logout == "default") {
			?>
			<div class="container col-10 col-md-5">
				<div class="alert alert-success">
					<p>Odhlášení proběhlo úspěšně.</p>
				</div>
			</div>
			<?php
		}
		elseif ($this->logout == "timeout") {
			?>
			<div class="container col-10 col-md-5">
				<div class="alert alert-primary">
					<p>Byli jste odhlášeni z důvodu delší neaktivity.</p>
				</div>
			</div>
			<?php
		}
		else {
			header("Location: index.php");
		}
		$this->getHTMLFooter();
	}

	/**
	 * Sets {@link logout}.
	 *
	 * @param string $logout the logout way
	 */
	public function setLogout($logout) {
		$this->logout = $logout;
	}
}