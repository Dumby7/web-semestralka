<?php

/**
 * The default, entry page of the web application.
 */
class MainPage extends Page {

	/** @var array Contributions of the users. */
	private $contributions;

	public function render() {
		$this->getHTMLHeader($this->webPages['main']['title']);
		$this->getMainMenu();
		?>
		<div class="table-responsive py-md-4">
			<table class="table table-striped table-hover">
				<caption><h2>Příspěvky uživatelů</h2></caption>

				<thead class="thead-dark">
				<tr>
					<th>Autor</th>
					<th>Soubor</th>
					<th>Název filmu</th>
					<th>Shrnutí</th>
					<th>Hodnocení</th>
				</tr>
				</thead>

				<tbody>
				<?php
				foreach ($this->contributions as $contr) {
					if ($contr["public"] == 0) {
						continue;
					}
					if ($contr["ban"] == 1) {
						continue;
					}

					$author = $contr["name"];
					$authorID = $contr["idUser"];
					$fileName = $contr["fileName"];
					$filePath = CONTRIBUTION_DIR . "/" . $authorID . "/" . $fileName;
					$movieTitle = $contr["movieTitle"];
					$content = $contr["content"];
					$rating = round($contr["overallRating"], 2);

					$modalID = "modal" . $contr["idContribution"];
					?>
					<tr>
						<td><?php echo $author ?></td>
						<td><a href="<?php echo $filePath ?>"><?php echo $fileName ?></a></td>
						<td><?php echo $movieTitle ?></td>
						<td>
							<!-- Button to Open the Modal -->
							<button type="button" class="btn btn-primary" data-toggle="modal"
							        data-target="#<?php echo $modalID ?>">
								Zobrazit shrnutí příspěvku
							</button>

							<!-- The Modal -->
							<div class="modal" id="<?php echo $modalID ?>">
								<div class="modal-dialog">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">Autorův souhrn příspěvku</h4>
											<button type="button" class="close" data-dismiss="modal">&times;
											</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<?php echo $content ?>
										</div>

										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">
												Zavřít
											</button>
										</div>

									</div>
								</div>
						</td>
						<td><?php echo $rating ?></td>
					</tr>
					<?php
				}
				?>
				</tbody>

			</table>
		</div>
		<?php
		$this->getHTMLFooter();
	}

	/**
	 * @param mixed $contributions
	 */
	public function setContributions($contributions) {
		$this->contributions = $contributions;
	}
}