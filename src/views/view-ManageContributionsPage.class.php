<?php

/**
 * Class that represents the template page where the admin can manage the contributions added in the application
 * by the users.
 */
class ManageContributionsPage extends Page {

	/** @var array The contributions information stored in the database, contains the info about the author. */
	private $contributions;
	/** @var array The reviewers that are registered in the application. */
	private $reviewers;
	/** @var array All of the ratings in the application. */
	private $ratings;


	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages['manageContributions']['title']);

		$this->getMainMenu();

		if ($this->user->isLoggedIn() && $this->user->getAuthorizationName() == "Administrator") {
			//TODO filtr tabulky
			?>
			<div class="table-responsive py-md-4">
				<?php $this->renderErrors();
				$this->renderSuccessMsg(); ?>

				<table class="table table-hover table-striped">
					<caption><h2>Správa příspěvků</h2></caption>

					<thead class="thead-dark">
					<tr>
						<th scope="col">ID příspěvku</th>
						<th scope="col">Soubor</th>
						<th scope="col">Autor</th>
						<th scope="col">Název filmu</th>
						<th scope="col">Datum přidání</th>
						<th scope="col">Hodnocení</th>
						<th scope="col">Veřejný</th>
						<th scope="col">Recenzenti</th>
						<th scope="col" colspan="3">Akce</th>
					</tr>
					</thead>

					<tbody>
					<?php
					foreach ($this->contributions as $contr) {
						// Skipping the contributions of the user that is logged in
						if ($contr["idUser"] == $this->getUser()->getID()) {
							continue;
						}
						// Skipping contributions from banned users
						if ($contr["ban"] == 1) {
							continue;
						}
						// Skipping declined contributions
						if ($contr["declined"] == 1) {
							continue;
						}

						$contrID = $contr["idContribution"];
						$formID = "form" . $contrID; // ID of the form that will be send out by one of the action
						$userID = $contr["idUser"]; // ID of the author of this contribution
						$userLogin = $contr["login"]; // Login of the author
						$userName = $contr["name"]; // Name of the author
						$fileName = $contr["fileName"];
						$path = CONTRIBUTION_DIR . "/" . $userID . "/" . $fileName; // Relative path to the file of the contribution
						$authorInfo = "ID:" . $userID . ($userName == "" ? " Login-" . $userLogin : " Jméno-" . $userName);
						$movieTitle = $contr["movieTitle"];
						$date = $contr["addDate"];
						$public = $contr["public"] == 1 ? true : false;
						$assigned = $contr["assigned"] == 1 ? true : false;
						$ratingsAmount = $contr["ratingsAmount"];
						$overallRating = $contr["overallRating"] == null ? "Nehodnocen" : round($contr["overallRating"], 2);
						// Allowing the admin to publish the contribution only after it has been rated at least 3 times
						$canPublish = $ratingsAmount >= 3 ? true : false;

						?>
						<tr>
							<td>
								<form id="<?php echo $formID ?>" method="post">
									<input type="hidden" name="contrID" value="<?php echo $contrID ?>">
								</form>
								<?php echo $contrID ?>
							</td>
							<td>
								<a href="<?php echo $path ?>"><?php echo $fileName ?></a>
							</td>
							<td><?php echo $authorInfo ?></td>
							<td><?php echo $movieTitle ?></td>
							<td><?php echo $date ?></td>
							<td>
								Počet hodnocení: <?php echo $ratingsAmount ?><br>
								Celkové hodnocení: <?php echo $overallRating ?>
							</td>
							<td><?php echo $public ? "ANO" : "NE" ?></td>
							<td>
								<?php
								if (!$assigned) {
									$this->selectReviewers($formID, $userID);
								}
								else {
									$this->showReviewers($contrID);
								}
								?>
							</td>
							<!--Button to assign the reviewers to the contribution, is disabled when the contribution has been assigned.-->
							<td>
								<input form="<?php echo $formID ?>" type="submit" name="assign" class="btn btn-primary"
								       value="Přiřadit" <?php echo $assigned ? "disabled" : "" ?>>
							</td>
							<!--Button to toggle the public status of the contribution.-->
							<td>
								<?php
								if (!$public) {
									?>
									<input form="<?php echo $formID ?>" type="submit" name="publish"
									       class="btn btn-success" value="Zveřejnit" <?php echo $canPublish ? "" : "disabled" ?>>
									<?php
								}
								else {
									?>
									<input form="<?php echo $formID ?>" type="submit" name="unPublish"
									       class="btn btn-success" value="Znepřístupnit">
									<?php
								}
								?>
							</td>
							<!--Button to decline the users contribution.-->
							<td>
								<input form="<?php echo $formID ?>" type="submit" name="decline" class="btn btn-danger"
								       value="Zamítnout">
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<?php
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>K této stránce nemáte přístup.</h3>
				</div>
			</div>
			<?php
			// Redirecting the user back to the main page after 3 seconds
			header("refresh: 3; url=index.php");
		}
		$this->getHTMLFooter();
	}

	/**
	 * @param mixed $contributions
	 */
	public function setContributions($contributions) {
		$this->contributions = $contributions;
	}

	/**
	 * @param mixed $reviewers
	 */
	public function setReviewers($reviewers) {
		$this->reviewers = $reviewers;
	}

	/**
	 * @param mixed $ratings
	 */
	public function setRatings($ratings) {
		$this->ratings = $ratings;
	}

	/**
	 * Creates a select box that shows all the reviewers that are registered in the application, except those that
	 * are banned, and not the author of the contribution.
	 *
	 * @param string $formID the id of the form that this select box will belong to
	 * @param string $authorID the id of the author, to be able to skip in case the reviewer is the author
	 */
	private function selectReviewers($formID, $authorID) {
		$selectBoxID = "select" . $formID;
		?>
		<select name="reviewersIDs[]" id="<?php echo $selectBoxID ?>" class="form-control custom-select"
		        form="<?php echo $formID ?>" size="3" multiple title="Select reviewers">
			<?php
			foreach ($this->reviewers as $reviewer) {
				// Skipping banned reviewers
				if ($reviewer["ban"] == 1) {
					continue;
				}
				// If the reviewer is the author of the contribution, skip him so he can't be assigned to review his own post
				if ($reviewer["idUser"] == $authorID) {
					continue;
				}
				$info = "ID:" . $reviewer["idUser"] . " - " . ($reviewer["name"] != "" ? $reviewer["name"] : $reviewer["login"]);
				?>
				<option value="<?php echo $reviewer['idUser'] ?>"><?php echo $info ?></option>
				<?php
			}
			?>
		</select>
		<?php
	}

	/**
	 * Shows the reviewers assigned to review the contribution.
	 *
	 * @param string $contrID id of the contribution that the reviewers are assigned to
	 */
	private function showReviewers($contrID) {
		foreach ($this->ratings as $rating) {
			// Finding the contribution id in the reviews
			if ($rating["idContribution"] == $contrID) {
				// Finding the reviewers that are paired with the contribution
				foreach ($this->reviewers as $reviewer) {
					if ($reviewer["idUser"] == $rating["idUser"]) {
						$reviewerName = $reviewer["name"] != "" ? $reviewer["name"] : $reviewer["login"];
						echo "ID:$reviewer[idUser] - $reviewerName<br>";
					}
				}
			}
		}
	}

}