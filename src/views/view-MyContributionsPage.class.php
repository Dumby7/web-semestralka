<?php

/**
 * Class that represents the template where the user can view the state of his own contributions.
 */
class MyContributionsPage extends Page {

	/** @var array Contributions that the user added to the application. */
	private $contributions;

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages['myContributions']['title']);

		$this->getMainMenu();
		if ($this->getUser()->isLoggedIn()) {
			if (!empty($this->contributions)) {
				?>
				<div class="table-responsive py-md-4">
					<?php $this->renderErrors() ?>
					<?php $this->renderSuccessMsg() ?>
					<table class="table table-hover table-striped">
						<caption><h2>Moje příspěvky</h2></caption>

						<thead class="thead-dark">
						<tr>
							<th scope="col">Soubor</th>
							<th scope="col">Název filmu</th>
							<th scope="col">Stav</th>
							<th scope="col">Hodnocení</th>
							<th scope="col">Smazat</th>
						</tr>
						</thead>

						<tbody>
						<?php
						foreach ($this->contributions as $contr) {
							$contrID = $contr["idContribution"];
							$formID = "form" . $contrID;
							$userID = $this->getUser()->getID();
							$movieTitle = $contr["movieTitle"];
							$fileName = $contr["fileName"];
							$path = CONTRIBUTION_DIR . "/" . $userID . "/" . $fileName;
							$rating = $contr["overallRating"];
							if ($rating === null) {
								$rating = "Nehodnocen";
							}
							else {
								$rating = round($rating, 2);
							}

							$state = $this->getState($contr);
							$style = $this->getStyle($contr);

							?>
							<tr <?php echo $style ?>>
								<td>
									<form id="<?php echo $formID ?>" method="post">
										<input type="hidden" name="contrID" value="<?php echo $contrID ?>">
										<input type="hidden" name="filePath" value="<?php echo $path ?>">
									</form>
									<a href="<?php echo $path ?>"><?php echo $fileName ?></a>
								</td>
								<td><?php echo $movieTitle ?></td>
								<td><?php echo $state ?></td>
								<td><?php echo $rating ?></td>
								<td>
									<input form="<?php echo $formID ?>" type="submit" class="btn btn-danger"
									       name="delete" value="Smazat">
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>

					</table>
				</div>
				<?php
			}
			else {
				?>
				<div class="container">
					<div class="row justify-content-center align-items-center invalidAccess">
						<h3>Aktuálně nemáte žádné přidané příspěvky.</h3><br>
					</div>
					<div class="row justify-content-center align-content-center">
						<small><a href="index.php?page=addContribution">Přidat příspěvek.</a></small>
					</div>
				</div>
				<?php
			}
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>Pro zobrazení této stránky musíte být přihlášen.</h3>
				</div>
			</div>
			<?php
		}
		$this->getHTMLFooter();
	}

	/**
	 * Contributions of the logged in user.
	 *
	 * @param array $contributions the contributions
	 */
	public function setContributions($contributions) {
		$this->contributions = $contributions;
	}

	/**
	 * Returns the string information about the state of the contribution.
	 *
	 * @param array $contr info about the contribution
	 * @return string the state
	 */
	private function getState($contr) {
		$state = "";
		$assigned = $contr["assigned"] == 1 ? true : false;
		$rated = $contr["ratingsAmount"] >= 3 ? true : false;
		$public = $contr["public"] == 1 ? true : false;
		$declined = $contr["declined"] == 1 ? true : false;

		if ($assigned) {
			$state = "Přiřazen k hodnocení";
		}
		else {
			$state = "Čeká na přiřazení";
		}

		if ($rated) {
			$state = "Čeká na rozhodnutí";
		}

		if ($public) {
			$state = "Příspěvek je veřejný";
		}

		if ($declined) {
			$state = "Příspěvek byl zamítnut";
		}

		return $state;
	}

	/**
	 * Returns the bootstrap style of the row of the contribution.
	 * If the contribution is declined returns the danger class if it is public returns success, else returns
	 * empty string.
	 *
	 * @param array $contr info about the contribution
	 * @return string the style
	 */
	private function getStyle($contr) {
		$style = "";
		if ($contr["public"] == 1) {
			$style = "class='table-success'";
		}
		if ($contr["declined"] == 1) {
			$style = "class='table-danger'";
		}

		return $style;
	}
}