<?php

/**
 * Abstract class that represents a generic page.
 */
abstract class Page {

	/** @var User User of the web application. */
	protected $user;
	/** @var array Errors that occurred when the user send a form. */
	protected $errors;
	/** @var string Message that should be set on a successful form send out. */
	protected $successMsg;
	/** @var array The available web pages in the application. */
	protected $webPages;
	/** @var string The current page of the application. */
	private $active;

	/**
	 * Page constructor.
	 */
	public function __construct() {
		global $webPages;
		$this->webPages = $webPages;
		$this->user = new User();

		// Loging out the user if he was inactive for 10 minutes
		if ($this->user->isLoggedIn()) {
			$inactive = 600;
			if (!isset($_SESSION['timeout'])) {
				$_SESSION['timeout'] = time() + $inactive;
			}

			$session_life = time() - $_SESSION['timeout'];

			if ($session_life > $inactive) {
				// The user was inactive for more than 10 minutes, redirecting to the logout page
				$_SESSION["logout"] = "timeout";
				header("Location:index.php?page=logout");
			}

			$_SESSION['timeout'] = time();
		}
	}

	/**
	 * Returns the user of the web application.
	 *
	 * @return User the user
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Sets new errors that occurred during the registration.
	 *
	 * @param array $errors array of errors that occurred during the registration
	 */
	public final function setErrors($errors) {
		if (!empty($errors) && is_array($errors)) {
			$this->errors = $errors;
		}
	}

	/**
	 * Sets the new success message.
	 *
	 * @param string $successMsg the new message
	 */
	public final function setSuccessMsg($successMsg) {
		if (!empty($successMsg)) {
			$this->successMsg = $successMsg;
		}
	}

	/**
	 * Generates the header structure of the HTML page. With the basic setting in the <head> tag.
	 * Has to be combined with the {@link getHTMLFooter} method in order for the HTML code to be complete.
	 *
	 * @param $title - the title of the page, cannot be null
	 */
	protected final function getHTMLHeader($title) {
		?>
		<!DOCTYPE html>
		<html lang="cs">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta name="author" content="Jakub Kodera">
			<meta name="description" content="Semestrální práce z KIV/WEB, konference publikací různých článků."/>
			<meta name="keywords" content="Semestrální práce, Konference, Články, Příspěvky, Hodnocení"/>

			<!--Bootstrap-->
			<link href="styles/bootstrap/css/bootstrap.min.css" rel="stylesheet">
			<!-- jQuery library -->
			<script src="styles/jquery/jquery-3.3.1.js"></script>
			<!--Bootstrap JavaScript-->
			<script src="styles/bootstrap/js/bootstrap.js"></script>

			<!--Icons-->
			<link href="styles/fontawesome/css/all.css" rel="stylesheet">

			<!--Custom styles-->
			<link rel="stylesheet" href="styles/style.css">

			<title><?php echo $title; ?> | Filmová konference</title>
		</head>

		<body>
		<div class="container-fluid" id="wrapper">
		<div class="jumbotron">
			<h1><a href="index.php">Filmová Konference</a></h1>
			<small>Přidávání a hodnocení příspěvků o filmech všeho druhu</small>
		</div>
		<?php
	}

	/**
	 * Generates the navigation menu for the web application.
	 */
	protected final function getMainMenu() {
		// Setting the current page to active
		if (isset($_GET["page"])) {
			$this->active = $_GET["page"];
		}
		else {
			$this->active = DEFAULT_PAGE;
		}
		?>

		<div id="navBar">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

				<!--The hamburger menu button that shows up on the menu when the screen is too small.-->
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNav">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="collapseNav">
					<!-- Left side of the menu. -->
					<ul class="navbar-nav">
						<?php
						foreach (PAGES as $page => $PAGE_INFO) {
							// Setting the class active argument for the page that the user is currently on
							echo "<li class='nav-item " . ($page == $this->active ? "active" : "") . "'>
								  <a class='nav-link' href='index.php?page=$page'>$PAGE_INFO[nav]</a></li>";
						}
						?>
					</ul>

					<!-- Right side of the menu. -->
					<ul class="nav navbar-nav ml-auto justify-content-end">
						<?php
						// If the user isn't logged in, displays the pages for logging in and registration
						if (!$this->user->isLoggedIn()) {
							foreach (GET_IN_PAGES as $page => $PAGE_INFO) {
								$icon = null;
								switch ($page) {
									case "log":
										$icon = "<i class='fas fa-user menu-icon'></i>";
										break;
									case "reg":
										$icon = "<i class='fas fa-sign-in-alt menu-icon'></i>";
								}
								echo "<li class='nav-item " . ($page == $this->active ? "active" : "") . "'>
									  <a class='nav-link' href='index.php?page=$page'>$icon $PAGE_INFO[nav]</a></li>";
							}
						}

						// The user is logged in, showing his name or login and his authorization name as a dropdown
						else {
							$userString = ""; // Label for the dropdown
							if ($this->user->getName() != "") {
								$userString .= $this->user->getName();
							}
							else {
								$userString .= $this->user->getLogin();
							}
							$userString .= " - ";
							// Changing the english names to czech equivalents
							switch ($this->user->getAuthorizationName()) {
								case "Administrator":
									$userString .= "Admin";
									break;
								case "Reviewer":
									$userString .= "Recenzent";
									break;
								case "Author":
									$userString .= "Autor";
									break;
							}

							/* Pages that are in the dropdown menu, for checking if the current active page is one
							 of the dropdown pages to style the label.
							 */
							$dropdownPages = array_merge(CONTRIBUTION_PAGES, USER_PAGES);
							?>

							<!-- The dropdown link with the user info, that upon click shows the user pages. -->
							<li class="nav-item dropdown <?php echo key_exists($this->active, $dropdownPages) ? "active" : "" ?>">
								<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
									<?php echo $userString ?>
								</a>
								<!-- The dropdown menu with the contribution and user pages. -->
								<div class="dropdown-menu">
									<?php
									// Contribution pages
									foreach (CONTRIBUTION_PAGES as $page => $PAGE_INFO) {
										// Not showing the link for managing the contributions if the user is not an administrator
										if ($page == "manageContributions" && $this->user->getAuthorizationName() != "Administrator") {
											continue;
										}
										// Not showing the link to rate the contributions if the user is not a reviewer
										if ($page == "rateContributions" && $this->user->getAuthorizationName() != "Reviewer") {
											continue;
										}
										echo "<a class='dropdown-item' href='index.php?page=$page'>$PAGE_INFO[nav]</a>";
									}

									// user pages for settings and managing
									foreach (USER_PAGES as $page => $PAGE_INFO) {
										// If the user is not an administrator skipping the link for managing users
										if ($page == "usermanage" && $this->user->getAuthorizationName() != "Administrator") {
											continue;
										}
										echo "<a class='dropdown-item' href='index.php?page=$page'>$PAGE_INFO[nav]</a>";
									}
									?>
									<hr>

									<!--Button to sign off the user, redirecting the form to the logout page.-->
									<form method="post" action="index.php?page=logout">
										<button type="submit" name="logout" class="dropdown-item btn btn-light">
											Odhlásit
										</button>
									</form>
								</div>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</nav>
		</div>
		<div class="content">
		<?php
	}

	/**
	 * Generates the end of the HTML page.
	 */
	protected final function getHTMLFooter() {
		?>
		<div class="footer-copyright text-center py-3">
			<footer>
				Jakub Kodera &copy; <?php echo date("Y"); ?>
			</footer>
		</div>
		</div>
		</div>
		<script>
            window.onscroll = function() {myFunction()};

            var navbar = document.getElementById("navBar");
            var sticky = navbar.offsetTop;

            function myFunction() {
                if (window.pageYOffset >= sticky) {
                    navbar.classList.add("sticky")
                } else {
                    navbar.classList.remove("sticky");
                }
            }
		</script>
		</body>
		</html>
		<?php
	}

	/**
	 * Generates the HTML code that shows all the errors to the user.
	 */
	protected function renderErrors() {
		if (!empty($this->errors)) {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center">
					<div class="col-10 col-md-5 alert alert-danger">
						<?php
						foreach ($this->errors as $error) {
							echo "<p>$error</p>";
						}
						?>
					</div>
				</div>
			</div>
			<?php
		}
	}

	/**
	 * Generates the HTML code that shows the {@link successMsg} to the user.
	 */
	protected function renderSuccessMsg() {
		if ($this->successMsg != "") {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center">
					<div class="col-10 col-md-5 alert alert-success">
						<?php echo "<p>$this->successMsg</p>" ?>
					</div>
				</div>
			</div>
			<?php
		}
	}

	/**
	 * Displays the HTML content of the website page.
	 */
	abstract public function render();
}