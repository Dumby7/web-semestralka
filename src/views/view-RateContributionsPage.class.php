<?php

/**
 * Class that represents the template where the user can review the contributions that have been assigned to him.
 */
class RateContributionsPage extends Page {

	/** @var int Maximum rating value. */
	const MAX_RATING = 5;
	/** @var array All of the contributions that have been assigned to the logged in reviewer that have not yet been published. */
	private $contributions;

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages['rateContributions']['title']);

		$this->getMainMenu();

		// Showing the main content only to logged in reviewers
		if ($this->getUser()->isLoggedIn() && $this->getUser()->getAuthorizationName() == "Reviewer") {
			if (!empty($this->contributions)) {
				?>
				<div class="table-responsive py-md-4">
					<?php $this->renderErrors() ?>
					<?php $this->renderSuccessMsg() ?>
					<table class="table table-hover table-striped">
						<caption><h2>Hodnocení příspěvků</h2></caption>

						<thead class="thead-dark">
						<tr>
							<th scope="col">Soubor</th>
							<th scope="col">Název filmu</th>
							<th scope="col">Obsah</th>
							<th scope="col" colspan="3">Hodnocení</th>
							<th scope="col">Hodnotit</th>
						</tr>
						</thead>

						<tbody>
						<?php
						foreach ($this->contributions as $contr) {
							$contrID = $contr["idContribution"];
							$formID = "form" . $contrID;
							$movieTitle = $contr["movieTitle"];
							$fileName = $contr["fileName"];
							$userID = $contr["idUser"]; // ID of the author of the contribution
							$path = CONTRIBUTION_DIR . "/" . $userID . "/" . $fileName; // path to the file on the server
							$content = $contr["content"];
							$themeRating = $contr["theme"];
							$originalityRating = $contr["originality"];
							$languageRating = $contr["language"];

							$alreadyRated = $themeRating != NULL ? true : false; // checking if the reviewer already rated the contribution

							// style for the row that has a contribution that has already been rated
							$reviewedStyle = $alreadyRated ? "class='table-success'" : "";

							$modalID = "modal" . $contrID; // id for the modal that contains the content
							?>
							<tr <?php echo $reviewedStyle ?>>
								<td>
									<form id="<?php echo $formID ?>" method="post">
										<input type="hidden" name="contrID" value="<?php echo $contrID ?>">
									</form>
									<a href="<?php echo $path ?>"><?php echo $fileName ?></a>
								</td>
								<td><?php echo $movieTitle ?></td>
								<td>
									<!-- Button to Open the Modal -->
									<button type="button" class="btn btn-primary" data-toggle="modal"
									        data-target="#<?php echo $modalID ?>">
										Zobrazit shrnutí příspěvku
									</button>

									<!-- The Modal -->
									<div class="modal" id="<?php echo $modalID ?>">
										<div class="modal-dialog">
											<div class="modal-content">

												<!-- Modal Header -->
												<div class="modal-header">
													<h4 class="modal-title">Autorův souhrn příspěvku</h4>
													<button type="button" class="close" data-dismiss="modal">&times;
													</button>
												</div>

												<!-- Modal body -->
												<div class="modal-body">
													<?php echo $content ?>
												</div>

												<!-- Modal footer -->
												<div class="modal-footer">
													<button type="button" class="btn btn-danger" data-dismiss="modal">
														Zavřít
													</button>
												</div>

											</div>
										</div>
									</div>
								</td>
								<td>
									<?php $this->createSelectBox("themeRating" . $contrID, $formID, $themeRating, "Téma") ?>
								</td>
								<td>
									<?php $this->createSelectBox("originalRating" . $contrID, $formID, $originalityRating, "Originalita") ?>
								</td>
								<td>
									<?php $this->createSelectBox("languageRating" . $contrID, $formID, $languageRating, "Úroveň jazyka") ?>
								</td>
								<td>
									<?php
									if (!$alreadyRated) {
										?>
										<input form="<?php echo $formID ?>" type="submit" name="rate"
										       class="btn btn-success" value="Hodnotit">
										<?php
									}
									else {
										?>
										<input form="<?php echo $formID ?>" type="submit" name="changeRating"
										       class="btn btn-success" value="Změna hodnocení">
										<?php
									}
									?>
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
				<?php
			}
			else {
				?>
				<div class="container">
					<div class="row justify-content-center align-items-center invalidAccess">
						<h3>Aktuálně nemáte žádné příspěvky přiřazené k hodnocení.</h3>
					</div>
				</div>
				<?php
			}
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>K této stránce nemáte přístup.</h3>
				</div>
			</div>
			<?php
			// Redirecting the user back to the main page after 3 seconds
			header("refresh: 3; url=index.php");
		}
		$this->getHTMLFooter();
	}

	/**
	 * Sets the contribution for the reviewer to review.
	 *
	 * @param array $contributions the information about all of the contributions
	 */
	public function setContributions($contributions) {
		$this->contributions = $contributions;
	}

	/**
	 * Creates a select box where the reviewer can choose a rating from 0 to 5.
	 *
	 * @param string $ID the value of the id attribute of the select tag
	 * @param string $formID the id of the form that the select will send its data with
	 * @param string|int $selected the selected value of all the possible values
	 * @param string $label the label text for the select tag
	 */
	private function createSelectBox($ID, $formID, $selected, $label) {
		?>
		<label for="<?php echo $ID ?>"><?php echo $label ?></label>
		<select form="<?php echo $formID ?>" id="<?php echo $ID ?>"
		        name="<?php echo $ID ?>" class="form-control">
			<?php
			for ($i = 0; $i <= self::MAX_RATING; $i++) {
				?>
				<option <?php echo $i == $selected ? "selected" : "" ?>><?php echo $i ?></option>
				<?php
			}
			?>
		</select>
		<?php
	}
}