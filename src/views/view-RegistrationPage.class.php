<?php

/**
 * The page where the user can create a new account.
 */
class RegistrationPage extends Page {

	public function render() {
		$this->getHTMLHeader($this->webPages['reg']['title']);

		$this->getMainMenu();

		if (!$this->user->isLoggedIn()) {
			?>

			<div class="container">
				<div class="row justify-content-center align-items-center form-box">
					<form id="registrationForm" class="col-10 col-md-5" autocomplete="off" method="post">
						<h2>Registrace</h2>

						<div class="form-group text-box">
							<label for="name"><i class="fas fa-address-card"></i></label>
							<input id="name" placeholder="Jméno a příjmení" type="text" name="name"
							       maxlength="35" value="<?php echo @$_POST["name"]; ?>">
						</div>

						<div class="form-group text-box">
							<label for="login"><i class="fas fa-user"></i></label>
							<input id="login" placeholder="Přihlašovací jméno" type="text" name="login"
							       value="<?php echo @$_POST["login"]; ?>" required>
						</div>
						<?php
						if (isset($this->errors["usernameInvalid"])) {
							echo "<small class='form-text warning'>".$this->errors['usernameInvalid']."</small>";
						}
						if (isset($this->errors["usernameExists"])) {
							echo "<small class='form-text warning'>".$this->errors['usernameExists']."</small>";
						}
						?>

						<div class="form-group text-box">
							<label for="password"><i class="fas fa-lock"></i></label>
							<input id="password" placeholder="Heslo" type="password" name="password" required>
						</div>
						<?php
						if (isset($this->errors["passLength"])) {
							echo "<small class='form-text warning'>".$this->errors['passLength']."</small>";
						}
						?>

						<div class="form-group text-box">
							<label for="passwordCheck"><i class="fas fa-check"></i></label>
							<input id="passwordCheck" placeholder="Potvrzení hesla" type="password" name="passwordCheck"
							       required>
						</div>
						<?php
						if (isset($this->errors["passDiff"])) {
						echo "<small class='form-text warning'>".$this->errors['passDiff']."</small>";
						}
						?>

						<div class="form-group text-box">
							<label for="email"><i class="fas fa-at"></i></label>
							<input id="email" type="email" placeholder="E-mail" name="email"
							       value="<?php echo @$_POST["email"]; ?>"
							       required>
						</div>
						<?php
						if (isset($this->errors["emailInvalid"])) {
							echo "<small class='form-text warning'>".$this->errors['emailInvalid']."</small>";
						}
						if (isset($this->errors["emailExists"])) {
							echo "<small class='form-text warning'>".$this->errors['emailExists']."</small>";
						}
						?>

						<input type="submit" class="submit-btn" name="registration" value="Registrovat">
						<a href="index.php?page=log"><small id="emailHelp" class="form-text">Už zde mám založený účet.</small></a>
					</form>
				</div>
			</div>

			<?php
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>Přihlášený uživatel se nemůže znovu registrovat.</h3>
				</div>
			</div>
			<?php
		}
		$this->getHTMLFooter();
	}
}