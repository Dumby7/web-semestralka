<?php

/**
 * The page where an administrator can change the roles of the users and ban/unban them.
 */
class UserManagePage extends Page {

	/** @var array All of the users that had registered to this application. */
	private $users;
	/** @var array All of the authorization in this application. */
	private $auths;

	/**
	 * Sets the users of this website.
	 *
	 * @param array $users all the users in the database
	 */
	public function setUsers($users) {
		$this->users = $users;
	}

	/**
	 * Sets the authorizations.
	 *
	 * @param array $auths all the authorizations from the database
	 */
	public function setAuths($auths) {
		$this->auths = $auths;
	}

	public function render() {
		$this->getHTMLHeader($this->webPages["usermanage"]["title"]);
		$this->getMainMenu();

		if ($this->user->isLoggedIn() && $this->user->getAuthorizationName() == "Administrator") {
			?>
			<div class="table-responsive py-md-4">
				<?php $this->renderSuccessMsg(); ?>
				<table class="table table-hover table-striped">
					<caption><h2>Uživatelé aplikace</h2></caption>
					<thead class="thead-dark">
					<tr>
						<th scope="col">ID uživatele</th>
						<th scope="col">Jméno a příjmení</th>
						<th scope="col">Přihlašovací jméno</th>
						<th scope="col">E-mail</th>
						<th scope="col">Právo</th>
						<th scope="col">Úprava</th>
						<th scope="col">Ban</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($this->users as $user) {
						// Skipping the current logged in user
						if ($user["login"] == $this->getUser()->getLogin()) {
							continue;
						}
						// Adding bootstrap class danger to banned users to more easily differentiate them from others
						$banStyle = $user['ban'] == 1 ? "class='table-danger'" : "";
						?>
						<tr <?php echo $banStyle ?> >
							<td><?php echo $user["idUser"] ?></td>
							<td><?php echo $user["name"] ?></td>
							<td><?php echo $user["login"] ?></td>
							<td><?php echo $user["email"] ?></td>
							<td>
								<form id='form<?php echo $user["idUser"] ?>' method='post'>
									<input type='hidden' name='idUser' value='<?php echo $user["idUser"] ?>'>
									<input type='hidden' name='name' value='<?php echo $user["name"] ?>'>
									<input type='hidden' name='login' value='<?php echo $user["login"] ?>'>
									<input type='hidden' name='email' value='<?php echo $user["email"] ?>'>
									<?php echo $this->authSelectBox($user["authorizationName"]) ?>
								</form>
							</td>
							<td>
								<!--The submit button to send out the form with the user info.-->
								<input form='form<?php echo $user["idUser"] ?>' class='btn btn-success' type='submit'
								       name='userChanges'
								       value='Upravit'>
							</td>
							<td>
								<?php
								// Displaying the button for banning/unbanning the user based on if the user is banned or not
								if ($user["ban"] == 0) {
									?>
									<input form='form<?php echo $user["idUser"] ?>' class='btn btn-danger' type='submit'
									       name='ban'
									       value='Banovat'>
									<?php
								}
								else {
									?>
									<input form='form<?php echo $user["idUser"] ?>' class='btn btn-danger' type='submit'
									       name='unban'
									       value='Unbanovat'>
									<?php
								}
								?>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<?php
		}
		// The user is not logged in or is not an administrator
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>K této stránce nemáte přístup.</h3>
				</div>
			</div>
			<?php
			// Redirecting the user back to the main page after 3 seconds
			header("refresh: 3; url=index.php");
		}
		$this->getHTMLFooter();
	}

	/**
	 * Returns a string that contains the HTML code for select box of all the possible authorizations.
	 * The selected value will be the users authorization.
	 *
	 * @param string $selected the users authorization name
	 * @return string the HTML code
	 */
	private function authSelectBox($selected) {
		$selectBox = "<select class='form-control' name='idAuthorization'>";

		foreach ($this->auths as $auth) {
			// Adding selected tag to this option
			if ($selected == $auth['name']) {
				// Changing the output of the authorization name to czech
				switch ($auth["name"]) {
					case "Administrator":
						$auth["name"] = "Admin";
						break;
					case "Author":
						$auth["name"] = "Autor";
						break;
					case "Reviewer":
						$auth["name"] = "Recenzent";
						break;
				}

				$selectBox .= "<option selected value='$auth[idAuthorization]'>$auth[name]</option>";
			}
			else {

				switch ($auth["name"]) {
					case "Administrator":
						$auth["name"] = "Admin";
						break;
					case "Author":
						$auth["name"] = "Autor";
						break;
					case "Reviewer":
						$auth["name"] = "Recenzent";
						break;
				}

				$selectBox .= "<option value='$auth[idAuthorization]'>$auth[name]</option>";
			}
		}
		$selectBox .= "</select>";

		return $selectBox;
	}

}