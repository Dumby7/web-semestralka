<?php

/**
 * The page where the user can change his current settings.
 */
class UserSettingsPage extends Page {

	/**
	 * Displays the HTML content of the website page.
	 */
	public function render() {
		$this->getHTMLHeader($this->webPages["usersettings"]["title"]);
		$this->getMainMenu();

		if ($this->user->isLoggedIn()) {

			?>

			<div class="container">
				<div class="row justify-content-center align-items-center form-box">
					<?php $this->renderSuccessMsg(); ?>
					<form class="col-10 col-md-5" method="post">
						<h2>Úprava osobních údajů</h2>

						<div class="form-group text-box">
							<label>Jméno a příjmení
								<input type="text" name="name" maxlength="35"
								       value="<?php echo $this->user->getName() ?>">
							</label>
						</div>

						<div class="form-group text-box">
							<label>Nové heslo
								<input type="password" name="newPass">
							</label>
						</div>
						<?php
						if (isset($this->errors["passLength"])) {
							echo "<small class='form-text warning'>" . $this->errors['passLength'] . "</small>";
						}
						?>

						<div class="form-group text-box">
							<label>Potvrzení nového hesla
								<input type="password" name="confPass">
							</label>
						</div>
						<?php
						if (isset($this->errors["passDiff"])) {
							echo "<small class='form-text warning'>" . $this->errors['passDiff'] . "</small>";
						}
						?>

						<div class="form-group text-box">
							<label>E-mailová adresa
								<input type="text" name="email" value="<?php echo $this->user->getEmail() ?>">
							</label>
						</div>
						<?php
						if (isset($this->errors["emailInvalid"])) {
							echo "<small class='form-text warning'>" . $this->errors['emailInvalid'] . "</small>";
						}
						if (isset($this->errors["emailExists"])) {
							echo "<small class='form-text warning'>" . $this->errors['emailExists'] . "</small>";
						}
						?>

						<input class="submit-btn" type="submit" name="updateSettings" value="Upravit">
					</form>
				</div>
			</div>
			<?php
		}
		else {
			?>
			<div class="container">
				<div class="row justify-content-center align-items-center invalidAccess">
					<h3>K zobrazení této stránky musíte být přihlášen.</h3>
				</div>
			</div>
			<?php
		}

		$this->getHTMLFooter();
	}
}